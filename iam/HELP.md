# Identity and Authorization Management

Persistens för IAM

## Komma igång

Du måste skapa en databas som heter *iam* för att kunna köra applikationen

    CREATE DATABASE iam;
    
## Valiga fel

    Connection error - Database inte igång eller porten felkonfigurerad
    Authorisation error - lösenordet fel (troligtvis) eller användanamnet
    Error creating bean with name 'userRepository' - Förmodlign så är inte databsaen skapad     
        
## Databasmigreringen heter Flyway
    https://www.baeldung.com/database-migrations-with-flyway

## maven invalid target release:

    export JAVA_HOME=/opt/java/jdk-14.0.1/
    
## Build

    maven clean install
    docker build -t java20/user-admin-server .
    docker save java20/user-admin-server > user-admin-server.tar
    
## Start docker db

    docker run -d --name user-admin-db -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password mysql    
    
## Start docker server

    docker run -d --name user-admin-server -p 8090:8080 -e MYSQL_HOST=192.168.1.46 java20/user-admin-server
    
    scp -i /home/sensera/.ssh/prod.pem user-admin-server.tar ubuntu@35.158.133.154:.