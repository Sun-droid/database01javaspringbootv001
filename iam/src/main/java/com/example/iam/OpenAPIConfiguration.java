package com.example.iam;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        servers = {
                @Server(
                        url = "http://localhost:8080",
                        description = "Dev server"
                ),
                @Server(
                        url = "https://usersjava20.sensera.se",
                        description = "Test server"
                )
        }
)
public class OpenAPIConfiguration {
}
