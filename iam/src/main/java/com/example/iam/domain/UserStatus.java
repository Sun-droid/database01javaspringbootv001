package com.example.iam.domain;

public enum UserStatus {
    EMAIL_VALIDATION,
    BLOCKED,
    ACTIVE
}
