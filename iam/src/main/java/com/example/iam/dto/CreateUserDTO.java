package com.example.iam.dto;

import lombok.Value;

@Value
public class CreateUserDTO {
    String username;
    String password;

}
