package com.example.iam.dto;

import lombok.Value;

@Value
public class RightsDTO {
    String id;
    String key;
    String domain;
}
