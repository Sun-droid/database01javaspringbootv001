package com.example.iam.dto;

import lombok.Value;

import java.util.List;

@Value
public class UserDTO {
    String id;
    String username;
    List<String> rights;
}
