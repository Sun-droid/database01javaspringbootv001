package com.example.iam.repo;

import com.example.iam.domain.Rights;
import com.example.iam.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface RightsRepository extends CrudRepository<Rights,String> {


}
